import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'

describe('App component', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(App)
  })

  afterEach(() => {
    wrapper.destroy()
  })

  test('should be a vue instance', () => {
    expect(wrapper.vm).toBeDefined()
  })

  test('checks if App has navigation components', () => {
    expect(wrapper.findComponent('v-navigation-drawer').exists()).toBeTruthy()
    expect(wrapper.findComponent('v-list-item').exists()).toBeTruthy()
    expect(wrapper.findComponent('v-list-item-content').exists()).toBeTruthy()

    const vListItemTitleComponent = wrapper.findComponent('v-list-item-title')
    expect(vListItemTitleComponent.exists()).toBeTruthy()
    expect(vListItemTitleComponent.text()).toBe('Pengurus')

    const vListItemSubtitleComponent = wrapper.findComponent('v-list-item-subtitle')
    expect(vListItemSubtitleComponent.exists()).toBeTruthy()
    expect(vListItemSubtitleComponent.text()).toBe('Gestão Financeira')
  })

  test('checks all menus is showing', () => {
    const vListComponent = wrapper.findComponent('v-list')
    expect(vListComponent.exists()).toBeTruthy()

    const vListMenuComponents = vListComponent.findAll('[color="primary"]')
    expect(vListMenuComponents.exists()).toBeTruthy()
    expect(vListMenuComponents).toHaveLength(3)

    let index = 0
    let titles = ['Dashboard', 'Planejamento', 'Investimentos']
    titles.forEach(title => {
      const vListComponent = vListMenuComponents.at(index)
      const componentTitle = vListComponent
        .findComponent('v-list-item-content')
        .findComponent('v-list-item-title')
      expect(componentTitle.text()).toBe(title)
      index++
    })
  })
})
