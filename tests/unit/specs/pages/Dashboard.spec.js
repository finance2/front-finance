import { shallowMount } from '@vue/test-utils'
import { jest } from '@jest/globals'
import moment from 'moment'
import Dashboard from '@/pages/dashboard/Dashboard.vue'

describe('Dashboard component', () => {
  let wrapper
  const methods = {
    loadMonth: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallowMount(Dashboard, {
      methods
    })
  })

  afterEach(() => {
    wrapper.destroy()
  })

  test('should be a vue coponent', () => {
    expect(wrapper.vm).toBeDefined()
  })

  test('should started with current monthYear', () => {
    const currentMonthYear = moment(new Date()).format('YYYY-MM')
    expect(wrapper.vm.monthYear).toBe(currentMonthYear)
  })

  test('should be render result category', () => {
    const result = wrapper.findComponent('#result')
    expect(result.exists()).toBeTruthy()
  })

  test('should be loads on change month', () => {
    const newMonthYear = '2022-06'
    const picker = wrapper.findComponent('#header > [label="Lançamentos do mês"]')
    expect(picker.exists()).toBeTruthy()

    picker.vm.$emit('change-month', newMonthYear)
    expect(methods.loadMonth).toHaveBeenCalledWith(newMonthYear)
  })

  test('should call adds new categoy', () => {
    const btnNewCategory = wrapper.findComponent('#header > v-btn')
    expect(btnNewCategory.exists()).toBeTruthy()

    btnNewCategory.trigger('click')
    expect(wrapper.vm.groups.length).toBe(1)
  })
})
