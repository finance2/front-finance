import { shallowMount } from '@vue/test-utils'
import { jest } from '@jest/globals'
import Planning from '@/pages/planning/Planning.vue'

const methods = {
    loadPlanning: jest.fn()
}

describe('Planning app', () => {
    let wrapper
    beforeEach(() => wrapper = shallowMount(Planning, {methods}))

    afterEach(() => wrapper.destroy())

    test('should started loads plannings', () => {
        expect(methods.loadPlanning).toHaveBeenCalledTimes(1)
    })

    test('should be render result category', () => {
        const result = wrapper.findComponent('#result')
        expect(result.exists()).toBeTruthy()
    })

    test('should call newCategory() and push on list', () => {
        const btnNewCategory = wrapper.findComponent('#header v-btn')
        expect(btnNewCategory.exists()).toBeTruthy()

        btnNewCategory.trigger('click')
        expect(wrapper.vm.categories.length).toBe(1)
    })
})