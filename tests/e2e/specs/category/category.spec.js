/// <reference types="Cypress" />

let categoryId = 0
let launchId = 0
const SERVER_API = Cypress.env('SERVER_API')
const currencyFormater = new Intl.NumberFormat('pt-BR', {currency: 'BRL', style: 'currency'})

describe('Category', () => {
    
    it('should be create a new income category', () => {
      const category = {
        title: 'Receita',
        type: 'Receita',
        planning: 15000
      }
      cy.intercept('GET', `${SERVER_API}/categories/*`, {fixture: 'categories'}).as('listCategories')
      cy.intercept('GET', `${SERVER_API}/planning/*`, {fixture: 'plannings'}).as('listPlannings')
      cy.intercept('POST', `${SERVER_API}/categories/`, {
        statusCode: 201,
        body: { id: ++categoryId }
      }).as('createCategory')
      cy.visit('/')
      registerNewCategory(category)

      cy.get('[data-cy="result-launch-description"]').first().should('have.text', category.title)
      cy.get('[data-cy="result-launch-planning"]').first().should('have.text', currencyFormater.format(category.planning))
      cy.get('[data-cy="result-launch-total"]').first().should('have.text', currencyFormater.format(0))
    })

    it('should be create a launch unique', () => {
      const launch = {
        description: 'Launch Unique',
        value: 500,
        status: 'Pago'
      }

      cy.intercept('POST', `${SERVER_API}/launches/`, {
        statusCode: 201,
        body: { id: ++launchId }
      })
      regiserNewLaunch(launch)

      cy.get('[data-cy="result-launch-total"]').should('have.text', currencyFormater.format(launch.value))
      cy.get('[data-cy="category-launches"]').children('tr').first().should('have.class', 'transparence')
    })

    it('should be change status launch', () => {
      cy.intercept('PUT', `${SERVER_API}/launches/`, (req) => {
        req.reply({
          statusCode: 201,
          body: { id: req.body.launch.id }
        })
      })
      cy.get('[data-cy="btn-status-change"]').click()
      
      cy.get('[data-cy="category-launches"]').first().should('not.have.class', 'transparence')
      cy.get('.status').should('have.text', 'Agendado')
      cy.get('[data-cy="btn-status-change"]').should('have.class', 'green--text')
    })

    it('should be update just one launch', () => {
      const launch = {
        description: 'Launch Unique Updated',
        value: 850,
        status: 'Atrasado'
      }
      cy.intercept('PUT', `${SERVER_API}/launches/`, (req) => {
        req.reply({
          statusCode: 201,
          body: { id: req.body.launch.id }
        })
      })
      cy.get('[data-cy="btn-update-launch"]').click()
      fillLaunch(launch)

      cy.get('[data-cy="modal-unique-title"]').should('have.text', 'A edição deve ser feita:')
      cy.get('[data-cy="btn-one-launch"]').click()

      cy.get('[data-cy="result-launch-total"]').should('have.text', currencyFormater.format(launch.value))
      cy.get('[data-cy="category-launches"]').first().should('not.have.class', 'transparence')
    })

    it('should be create a expense category', () => {
      const category = {
        title: 'Despesa',
        type: 'Despesa',
        planning: 2000
      }

      cy.intercept('POST', `${SERVER_API}/categories/`, {
        statusCode: 201,
        body: { id: ++categoryId }
      }).as('createCategory')
      registerNewCategory(category)

      cy.get('[data-cy="result-launch-description"]').eq(1).should('have.text', category.title)
      cy.get('[data-cy="result-launch-planning"]').eq(1).should('have.text', currencyFormater.format(category.planning))
      cy.get('[data-cy="result-launch-total"]').eq(1).should('have.text', currencyFormater.format(0))
    })

    it('should be create a repeated expense launch', () => {
      const indexDespesa = 1
      const launch = {
        description: 'Launch Repeated',
        value: 100,
        status: 'Agendado',
        setting: {
          option_frequency: 'repeat',
          total_installments: 5
        }
      }

      cy.intercept('POST', `${SERVER_API}/launches/`, {
        statusCode: 201,
        body: { id: ++launchId }
      })
      regiserNewLaunch(launch, indexDespesa)

      cy.get('[data-cy="result-launch-total"]').eq(indexDespesa).should('have.text', currencyFormater.format(-launch.value))
      const expenseCategory = cy.get('[data-cy="category-launches"]').eq(indexDespesa)
      expenseCategory.get('.info-installment').should('have.text', `(1/${launch.setting.total_installments})`)
      expenseCategory.should('not.have.class', 'transparence')
      expenseCategory.get('.status').eq(indexDespesa).should('have.text', 'Agendado')
    })

    it('should be remove a launch', () => {
      const indexDespesa = 1

      cy.intercept('DELETE', `${SERVER_API}/launches/${launchId}`, {
        statusCode: 200,
        body: { message: 'Launch successfully removed' }
      })

      let expenseCategory = cy.get('[data-cy="category-launches"]').eq(indexDespesa)
      expenseCategory.find('[data-cy="btn-remove-launch"]').click()

      expenseCategory = cy.get('[data-cy="category-launches"]').eq(indexDespesa)
      expenseCategory.children('tr').first().should('have.text', 'Nenhum lançamento registrado')
    })
  })


  function registerNewCategory(category) {
    cy.get('[data-cy="button-register-category"]').click()
    
    cy.get('#title').clear().type(category.title)
    
    cy.get('[data-cy="select-type"]').parent().click()
      .get(".v-menu__content").contains(category.type).click()

    cy.get('#planning').clear().type(category.planning)

    cy.get('[data-cy="btn-submit"]').click()
  }

  function regiserNewLaunch(launch, index = 0) {
    cy.get('[data-cy="btn-new-launch"]').eq(index).click()
    fillLaunch(launch)
  }

  function fillLaunch(launch) {
    cy.get('#description').clear().type(launch.description)

    cy.get('#value').clear().type(launch.value)

    cy.get('#status').parent().click()
      .get('.v-menu__content').contains(launch.status).click()
    
    if(launch.setting) {
      cy.get(`[value="${launch.setting.option_frequency}"][data-cy="radio-frequency"]`).parent().click()
      cy.get('#installments').clear().type(launch.setting.total_installments)
    }

    return cy.get('[type=submit]').click()
  }