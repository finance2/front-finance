/// <reference types="Cypress" />

const SERVER_API = Cypress.env('SERVER_API')
const currencyFormater = Intl.NumberFormat('pt-BR', {currency: 'BRL', style: 'currency'})

describe('Launches Parcel', () => {
    before(() => {
        cy.intercept('GET', `${SERVER_API}/categories/*`,{fixture: 'parcel_launches'}).as('categoriesParcelLaunches')
        cy.visit('/')
      
    })
    it('should be render modal confirmation change value', () => {
        cy.intercept('PUT', `${SERVER_API}/launches`, { statusCode: 201 })

        cy.get('[data-cy="btn-update-launch"]').first().click()
        cy.get('input#value-installment').should('have.value', 1000)

        cy.get('#value').clear().type('300')
        cy.get('[type=submit]').click()

        cy.get('.v-dialog').should('be.visible')
        cy.get('[data-cy="btn-all-launch"]').click()
    })
    it('should be render modal choice update planning launch or create new category launch', () => {
        cy.intercept('PUT', `${SERVER_API}/planning/launch/`, { statusCode: 201 })

        cy.get('[data-cy="btn-update-launch"]').eq(1).click()
        cy.get('#description').clear().type('Launch Planning Updated')
        cy.get('[type=submit]').click()

        const modal = cy.get('.v-dialog')
        modal.should('be.visible')
        modal.get('[data-cy="btn-just-launch"]').should('contain.text', 'Somente neste lançamento')
        modal.get('[data-cy="btn-update-planning"]').should('contain.text', 'Atualizar Planejamento').click()

        cy.get('[data-cy="category-launches"] > :nth-child(2)').should('contain.text', 'Launch Planning Updated')
    })
})