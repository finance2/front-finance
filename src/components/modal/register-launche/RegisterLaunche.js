import service from '@/domains/finance/services/index.js'

export default {
  created () {
    service.allCategories()
      .then(res => (this.categories = res.categories))
  },
  data () {
    return {
      dialog: false,
      dateMenu: false,
      color: 'pink',
      launche: newLaunche(),
      categories: [],
      methodPayments: ['Dinheiro', 'Cartão de Crédito', 'Cartão de Débito'],
      status: [
        {text: 'Agendado', value: 'SCHEDULED'},
        {text: 'Atrasado', value: 'OPENED'},
        {text: 'Pago', value: 'CLOSED'}
      ]
    }
  },
  methods: {
    save () {
      service
        .saveLaunche(this.launche)
        .then(savedLaunche => {
          savedLaunche.category = this.launche.category
          this.$emit('create-launche', savedLaunche)
          this.launche = newLaunche()
        })
      this.closeDialog()
    },
    closeDialog () {
      this.dialog = false
    }
  }
}

function newLaunche () {
  return { date: new Date().toISOString().substr(0, 10) }
}
