import service from '@/domains/finance/services/index.js'

export default {
    data () {
        return {
            category: {}
        }
    },
    methods: {
        save (category) {
            service.saveCategory(category)
                .then(newCategory => this.$emit('create-category', newCategory))
                .catch(error => console.error(error))
            this.category = {}
        }
    }
}