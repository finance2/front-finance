import { EVENT_BUS } from '@/support/util/event-bus.js'

export default {
    props: {
        categories: {type: Array, default: () => []}
    },
    methods: {
        isIncome(category) {
            return category.type === "INCOME"
        },
        totalCategory(category) {
            var total = total = category.launches.reduce((sum, launche) => sum + launche.value, 0);
            EVENT_BUS.$emit('totalByCategory', {name: category.name, total})
            return total
        },
    },
    computed: {
        categoriesWithLaunches() {
            return this.categories.filter(category => category.launches.length > 0)
        }
    }
}