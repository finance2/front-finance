
export default {
  props: ['info'],
  beforeMount: function () {
    this.flag = require(`@/assets/${this.info.flag}.png`)
    this.bkColor = {
      background: `linear-gradient(45deg, ${this.info.colors})`
    }
  },
  data: () => ({
    flag: undefined,
    bkColor: undefined,
    fab: false
  }),
  methods: {
    /**
     * @param {{
     *  id: string
     *  title: string
     * }} card
     */
    openCard (card) {
      const route = `/cards/${card.id}`
      if (this.$route.path !== route) {
        this.$router.push(route)
      }
    },

    editCard (card) {
      const route = `/cards/form/${card.id}`
      if (this.$route.path !== route) this.$router.push(route)
    }
  }
}
