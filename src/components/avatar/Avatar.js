export default {
  props: {
    name: {
      type: String,
      required: true,
      default: ''
    },
    profile: {
      type: String,
      required: true
    }
  }
}
