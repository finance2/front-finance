export default {
  props: ['groups'],
  computed: {
    result: function () {
      return this.groups.reduce((total, group) => (total += this.totalGroup(group)), 0)
    }
  },
  methods: {
    totalGroup (group) {
      const delta = this.isExpense(group) ? -1 : 1
      return group.launches.reduce(
        (total, launch) => (total += launch.value), 0) * delta
    },

    isExpense (group) {
      return group.type === 'expense'
    },

    isNegative (value) {
      return value < 0
    }
  }
}
