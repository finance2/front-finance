import {
  fromCurrency
} from '@/support/util/currency-util'

export default class LaunchValidator {
  descriptionRules = [
    value => !!value || 'Descrição é obrigatória'
  ]

  valueRules = [
    value => !!value || 'O valor é obrigatório',
    value => fromCurrency(value) > 0 || 'O valor deve ser maior que zero'
  ]
}
