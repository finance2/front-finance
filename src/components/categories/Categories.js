import Launches from './body/Launches.vue'
import AddLaunch from './body/AddLaunch.vue'
import Config from './body/Config.vue'

export default {
  components: {
    launches: Launches,
    addLaunch: AddLaunch,
    config: Config
  },
  props: ['group', 'currentMonth'],
  created: function () {
    this.title = this.group.title
  },
  beforeMount: function () {
    this.currentContent = this.group.content || 'launches'
  },
  data: () => ({
    launch: {},
    currentContent: undefined,
    items: [
      { title: 'Configurações', component: 'config' }
    ]
  }),
  methods: {
    swapContent (componentName, title) {
      this.currentContent = componentName
      this.title = title
    },
    editLaunch (launch) {
      this.launch = Object.assign({},
        launch,
        {setting: {...launch.setting}})
      this.swapContent('addLaunch', 'Edição Lançamento')
    }
  }
}
