import CategoryService from '@/services/category/CategoryService.js'
import LaunchService from '@/services/launch/LaunchService'
import { EVENT_BUS } from '@/support/util/event-bus.js'

const _categoryService = new CategoryService()
const _launchService = new LaunchService()

export default {
  created () {
    this._loadsCategoriesTitle(this.monthYear)
    EVENT_BUS.$on('editLaunch', launch => {
      this.launch = launch
      this.dialog = true
    })
  },

  props: ['monthYear'],

  data () {
    return {
      title: 'Teste',
      dialog: false,
      launch: {},
      categories: []
    }
  },

  watch: {
    monthYear (monthYear) {
      this._loadsCategoriesTitle(monthYear)
    }
  },

  methods: {
    saveLaunch () {
      this.launch.value = Number(this.launch.value)
      const method = this.launch._id ? 'updateLaunch' : 'saveLaunch'
      _launchService[method](this.launch)
        .then(this.reload)
    },

    reload () {
      const { categories } = this.$data
      Object.assign(this.$data, this.$options.data(), { categories })
    },

    _loadsCategoriesTitle (monthYear) {
      _categoryService
        .listCategoriesTitle(monthYear)
        .then(categories => (this.categories = categories))
    }
  }
}
