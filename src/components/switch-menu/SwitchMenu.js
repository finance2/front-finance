import moment from 'moment'
import { EVENT_BUS } from '@/support/util/event-bus.js'
import GroupService from '@/services/group/GroupService.js'

import DialogLaunch from '@/components/dialog-launch/DialogLaunch.vue'

const groupService = new GroupService()

export default {
  components: {
    DialogLaunch
  },
  created () {
    EVENT_BUS.$on('configGroup', group => {
      this.group = group
      this.drawer = !this.drawer
    })
  },

  data () {
    return {
      menus: [
        { title: 'Finanças', active: true },
        { title: 'Investimentos', active: false }
      ],
      date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
      menu: false,
      modal: false,
      menu2: false,
      drawer: false,
      group: { title: '', styles: { background: '#E15D3A', color: '#791d04' } }
    }
  },

  computed: {
    formatted () {
      return this.date ? moment(this.date).format('MM/YYYY') : ''
    }
  },

  methods: {
    selectMenu (menu) {
      this.menus.find(menu => menu.active).active = false
      menu.active = true
    },

    selectDate () {
      EVENT_BUS.$emit('changeDate', moment(this.date).format('MM/YYYY'))
    },

    saveGroup () {
      this.group._id
        ? groupService.updateGroup([this.group])
        : groupService.saveGroup(this.group, this.formatted)
      Object.assign(this.$data, this.$options.data(), {date: this.date})
    },

    newGroup () {
      this.drawer = !this.drawer
      this.group = { title: '', styles: { background: '#E15D3A', color: '#791d04' } }
    }
  }
}
