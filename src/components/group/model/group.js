export default {
  title: String,
  launches: Array,
  styles: {
    background: String,
    color: String
  }
}
