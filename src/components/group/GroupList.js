import { EVENT_BUS } from '@/support/util/event-bus.js'
import Group from './model/group'
import LaunchService from '@/services/launch/LaunchService'

const _launchService = new LaunchService()

export default {
  props: {
    group: Group
  },

  data: () => ({
    over: false
  }),

  methods: {
    editGroup () {
      EVENT_BUS.$emit('configGroup', this.group)
    },

    editLaunch (launch) {
      EVENT_BUS.$emit('editLaunch', launch)
    },

    switchToPay (launch) {
      launch.status = 'Pago'
      _launchService.updateLaunch(launch)
    }
  }
}
