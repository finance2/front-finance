import moment from 'moment'

export default {
  props: {
    label: { type: String, default: '' },
    max: String
  },
  created () {
    this.monthSelect = this.monthYear
  },
  data: () => ({
    menu: false,
    monthYear: new Date().toISOString().substr(0, 10)
  }),
  methods: {
    selectMonth (month) {
      this.$emit('change-month', month)
    }
  },
  computed: {
    computedDateFormattedMomentjs () {
      return this.monthYear ? this.$options.filters.capitalize(moment(this.monthYear).format('MMMM/YYYY')) : ''
    }
  }
}
