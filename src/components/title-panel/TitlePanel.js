import colors from "vuetify/lib/util/colors"

export default {
  props: {
    title: String,
    colors: Array
  },
  data () {
    return {
      gradientStyle: {
        background: this.colors.length > 1
          ? `linear-gradient(45deg, ${this.colors.toString()})`
          : this.colors[0]
      }
    }
  }
}
