import moment from 'moment'

export default {
  props: {
    label: { type: String, default: '' },
    currentDate: String,
    max: String
  },
  created () {
    this.date = this.currentDate || new Date().toISOString()
  },
  data: () => ({
    menu: false,
    date: undefined
  }),
  methods: {
    selectDate (date) {
      this.$emit('select-date', this._formatDate(date))
    },
    _formatDate (date) {
      return moment(date).format('DD/MM/YYYY')
    }
  },
  computed: {
    computedDateFormattedMomentjs () {
      return this.date ? this._formatDate(this.date) : ''
    }
  }
}
