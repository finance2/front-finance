// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'vuetify/dist/vuetify.min.css'

import Vue from 'vue'
import App from './App'
import router from './router'
import moment from 'moment'

import vuetify from './plugins/vuetify'

// Plugins
import GlobalComponents from './GlobalComponents'

// Filters
import GlobalFilters from './GlobalFilters'

// Directives
import GlobalDirectives from './GlobalDirective'

moment.locale('pt-br')

Vue.config.productionTip = false

Vue.use(GlobalComponents)
Vue.use(GlobalFilters)
Vue.use(GlobalDirectives)
/* eslint-disable no-new */
new Vue({
  vuetify,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
