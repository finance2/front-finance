import Sidebar from '@/components/sidebar/Sidebar.vue'
import Avatar from '@/components/avatar/Avatar.vue'
import TitlePanel from '@/components/title-panel/TitlePanel.vue'
import Picker from '@/components/picker/Picker.vue'
import SwitchMenu from '@/components/switch-menu/SwitchMenu.vue'
import DatePicker from '@/components/datePicker/DatePicker.vue'

const GlobalComponents = {
  install (Vue) {
    Vue.component('switchMenu', SwitchMenu)
    Vue.component('sidebar', Sidebar)
    Vue.component('avatar', Avatar)
    Vue.component('titlePanel', TitlePanel)
    Vue.component('picker', Picker)
    Vue.component('datePicker', DatePicker)
  }
}

export default GlobalComponents
