import {
  format,
  fromCurrency
} from './support/util/currency-util'

function set (obj, path, value) {
  var schema = obj // a moving reference to internal objects within obj
  var pList = path.split('.')
  var len = pList.length
  for (var i = 0; i < len - 1; i++) {
    var elem = pList[i]
    if (!schema[elem]) schema[elem] = {}
    schema = schema[elem]
  }

  schema[pList[len - 1]] = value
}

const GlobalDirectives = {
  install (Vue) {
    Vue.directive('currency', {
      inserted: (el) => {
        const input = el.getElementsByTagName('input').item(0)
        input.value = format(input.value)
        input.dispatchEvent(new CustomEvent('input'))
      },
      bind: (el, bind, vnode) => {
        el.getElementsByTagName('input').item(0).addEventListener('blur', element => {
          set(vnode.context, bind.expression, fromCurrency(element.target.value))
        })

        el.addEventListener('input', elem => {
          let value = fromCurrency(elem.target.value)
          elem.target.value = format(value)
          elem.target.dispatchEvent(new CustomEvent('input'))
        })
      }
    })
  }
}

export default GlobalDirectives
