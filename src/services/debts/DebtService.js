import { EVENT_BUS } from '@/support/util/event-bus.js'
import { nextWeekDate } from '../../support/util/date-util'

export default class DebtService {
  constructor () {
    this._debts = this._loadDebts()
    this._nextId = this.debts.length
    EVENT_BUS.$on('onChangeStatus', console.log)
  }

  get debts () {
    return this._debts
  }

  debtsByMonth (month) {
    month = !month ? new Date().toISOString().substr(0, 7) : month
    return this.debts.filter(({ date }) => {
      return date.substr(0, 7) === month
    })
  }

  saveDebt (debt) {
    let debts = this._loadDebts()
    debt.id = this._nextId++
    debts.push(debt)

    debts = debts
      .concat(this._createsPastDebts(debt))
      .concat(this._createsFutureDebts(debt))
    localStorage.setItem('debts', JSON.stringify(debts))
  }

  updateDebt (debt) {
    let debts = this._loadDebts()
    const indexDebt = debts.findIndex(({ id }) => id === debt.id)
    debts[indexDebt] = debt
    localStorage.setItem('debts', JSON.stringify(debts))
  }

  _loadDebts () {
    return JSON.parse(localStorage.getItem('debts')) || []
  }

  _createsFutureDebts (debt) {
    const amount = debt.totalInstallment - debt.installmentNumber
    return Array.from({length: amount}).map((_, index) => {
      const month = ++index
      const futureDebt = {...debt}
      futureDebt.id = this._nextId++
      futureDebt.installmentNumber = debt.installmentNumber + index
      futureDebt.date = nextWeekDate(month)
      return futureDebt
    })
  }

  _createsPastDebts (debt) {
    const amount = debt.installmentNumber - 1
    return Array.from({length: amount}).map((_, index) => {
      const month = index - amount
      const oldDebt = {...debt}
      oldDebt.id = this._nextId++
      oldDebt.status = 'Paid'
      oldDebt.installmentNumber = ++index
      oldDebt.date = nextWeekDate(month)
      return oldDebt
    })
  }
}
