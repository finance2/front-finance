import axios from 'axios'
import Service from '../Service'

export default class PlanningService extends Service {
  loadsPlanning (monthYear) {
    return axios
      .get(`${this.SERVER_API}/planning`, {params: {month: monthYear}})
      .then(response => response.data)
  }

  saveCategory (category) {
    return axios
      .post(`${this.SERVER_API}/planning/`, category)
  }

  updateCategory (category) {
    return axios
      .put(`${this.SERVER_API}/planning/`, category)
  }

  addLaunch (launch) {
    return axios
      .post(`${this.SERVER_API}/planning/launch/`, launch)
  }

  updateLaunch (launch) {
    return axios
      .put(`${this.SERVER_API}/planning/launch/`, launch)
  }

  prepareLaunch (launch) {
    if (['parcel'].includes(launch.setting.option_frequency)) {
      launch.value = launch.value * launch.setting.total_installments
    }
    return launch
  }
}
