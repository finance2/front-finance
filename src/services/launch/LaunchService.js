import axios from 'axios'
import Service from '../Service'
import { fromCurrency } from '@/support/util/currency-util'

export default class LaunchService extends Service {
  launchesByCategoryAndMonth (categoryId, monthYear) {
    return axios
      .get(`${this.SERVER_API}/category/${categoryId}`, {params: {month: monthYear}})
      .then(response => response.data)
  }

  saveLaunch (launch) {
    if (launch.setting.option_frequency === 'unique') {
      delete launch.date
    }

    return axios
      .post(`${this.SERVER_API}/launches/`, launch)
      .then(response => {
        launch.id = response.data.id
        return launch
      })
  }

  updateLaunch (launchChanged) {
    this._parseValues(launchChanged.launch)
    return axios.put(`${this.SERVER_API}/launches/`, launchChanged)
  }

  _parseValues (launch) {
    if (typeof launch.value === 'string' || launch.value instanceof String) {
      launch.value = fromCurrency(launch.value)
    }
  }

  removeLaunch (launch) {
    return axios
      .delete(`${this.SERVER_API}/launches/${launch.id}`)
  }
}
