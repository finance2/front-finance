import { EVENT_BUS } from '@/support/util/event-bus.js'
import axios from 'axios'

export default class GroupService {
  saveGroup (group, monthYear) {
    group.monthYear = monthYear
    group.launches = []
    axios
      .post(`${process.env.SERVER_API}/category/`, group)
      .then(response => {
        group._id = response.data.id
        return group
      })
      .then(group => EVENT_BUS.$emit('newGroup', group))
      .then(() => (EVENT_BUS.$emit('messages', {type: 'success', text: 'Grupo criado!'})))
      .catch(() => (EVENT_BUS.$emit('messages', {type: 'error', text: 'Não foi possível criar o grupo!'})))
  }

  updateGroup (groups) {
    axios
      .put(`${process.env.SERVER_API}/category/`, groups)
      .then(() => (EVENT_BUS.$emit('messages', {type: 'success', text: 'Grupo atualizado!'})))
      .catch(() => (EVENT_BUS.$emit('messages', {type: 'error', text: 'Não foi possível atualizar o grupo!'})))
  }

  listGroups (monthYear) {
    return axios
      .get(`${process.env.SERVER_API}/category/`, {params: { monthYear }})
      .then(response => response.data)
  }
}
