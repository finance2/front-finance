import moment from 'moment'
import axios from 'axios'
import Service from '../Service'

export default class CategoryService extends Service {
  get categores () {
    return this._categories || []
  }

  loadCategoriesCurrentMonth () {
    return this.loadCategoriesByMonth(moment().format('YYYY-MM'))
  }

  categoriesMonthYear (monthYear, showEmptyCategories = false) {
    return axios
      .get(`${this.SERVER_API}/category/${monthYear}?showEmptyCategories=${showEmptyCategories}`)
      .then(response => response.data)
  }

  categoryById (categoryId) {
    return axios
      .get(`${this._URL_API}/${categoryId}`)
      .then(response => response.data)
  }

  listCategoriesTitle (monthYear) {
    return axios
      .get(`${this._URL_API}/titles/`, {params: { monthYear }})
      .then(response => response.data)
  }

  _lastMonthAvailable () {
    // const currentMonth = moment()
    // const month = moment.max(
    //   this._categories
    //     .filter(({ monthYear }) => currentMonth.isSameOrAfter(monthYear))
    //     .map(({ monthYear }) => moment(monthYear))
    // ).format('YYYY-MM')

    return this.categories
  }

  saveCategory (category) {
    return axios.post(`${this.SERVER_API}/category/`, category)
  }

  updateCategory (category) {
    return axios.put(`${this.SERVER_API}/category/`, category)
  }
}
