import axios from 'axios'
import Service from '../Service'
import { fromCurrency } from '@/support/util/currency-util'

export default class CardService extends Service {
  cardById (cardId) {
    return axios
      .get(`${this.SERVER_API}/card/${cardId}`)
      .then(response => response.data)
  }

  createCard (card) {
    return axios
      .post(`${this.SERVER_API}/card/`, card)
      .then(response => response.data)
  }

  updateCard (card) {
    return axios
      .put(`${this.SERVER_API}/card/`, card)
  }

  removeCard (cardId) {
    return axios
      .delete(`${this.SERVER_API}/card/${cardId}`)
  }

  listCards (monthYear) {
    return axios
      .get(`${this.SERVER_API}/card`, {
        params: { monthYear }
      })
      .then(response => response.data)
  }

  launchesByCardId (cardId, monthYear) {
    return axios
      .get(`${this.SERVER_API}/card/launches`, {
        params: { cardId, monthYear }
      })
      .then(response => response.data)
  }

  addLaunch (launch) {
    return axios
      .post(`${this.SERVER_API}/card/launches`, launch)
      .then(response => {
        launch.id = response.id
        launch.setting.launch_id = response.id
        return launch
      })
  }

  updateLaunch (launch) {
    this._parseValues(launch)
    return axios
      .put(`${this.SERVER_API}/card/launches`, launch)
      .then(() => console.info(`Updating launch card ${launch}`))
  }

  _parseValues (launch) {
    if (typeof launch.value === 'string' || launch.value instanceof String) {
      launch.value = fromCurrency(launch.value)
    }
  }

  removeCardLaunch (launchId) {
    return axios
      .delete(`${this.SERVER_API}/card/launches/${launchId}`)
  }
}
