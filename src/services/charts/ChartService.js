import axios from 'axios'
import moment from 'moment'

function getLabels (month) {
  const endMonth = moment(month, 'MM/YYYY')
  return Array
    .apply(0, Array(12))
    .map((_, i) => endMonth.subtract(i ? 1 : 0, 'month').format('MMMM'))
    .map(camelCase)
    .reverse()
}

function camelCase (str) {
  return str[0].toUpperCase().concat(str.substring(1))
}

function dataset (launches, labels) {
  launches.forEach(launch => {
    launch.data = launch.data.reduce((obj, { value, month }) => {
      const launchMonth = camelCase(moment(month, 'MM/YYYY').format('MMMM'))
      const index = labels.findIndex(m => m === launchMonth)

      obj[index] = value
      return obj
    }, Array(12).fill(0))
  })
}

export default class ChartService {
  getBarDatas (monthYear) {
    return axios
      .get(`${process.env.SERVER_API}/category/chart/bar`, {params: { monthYear }})
      .then(response => response.data)
      .then(launches => ({ launches, labels: getLabels(monthYear) }))
      .then(({ launches, labels }) => {
        dataset(launches, labels)
        return { datasets: launches, labels }
      })
  }
}
