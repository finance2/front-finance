import moment from 'moment'

import Category from '@/components/categories/Categories.vue'
import Picker from '@/components/picker/Picker.vue'
import Result from '@/components/result/Result.vue'
import Card from '@/components/card/Card.vue'
import CategoryService from '@/services/category/CategoryService.js'
import CardService from '@/services/card/CardService.js'

const categoryService = new CategoryService()
const cardService = new CardService()

export default {
  components: {
    category: Category,
    picker: Picker,
    result: Result,
    card: Card
  },
  created: function () {
    this.monthYear = moment(new Date()).format('YYYY-MM')
    this.loadMonth(this.monthYear)
  },
  computed: {
    orderedCategory: function () {
      return this.groups
        .sort((g1, g2) => g1.position - g2.position)
        .concat(this.resumeCards)
    }
  },
  data: () => ({
    groups: [],
    showEmptyCategories: false,
    cards: [],
    resumeCards: [],
    fab: false
  }),
  methods: {
    newCard () {
      this.$router.push('/cards/form')
    },
    newCategory () {
      this.groups.push({
        monthYear: this.monthYear,
        content: 'config',
        launches: [],
        position: this.groups.length + 1,
        color: '#FF0000',
        planning: 0
      })
    },
    loadMonth (month, showEmptyCategories = false) {
      this.loadCategories(month, showEmptyCategories)
      this.listCards(month)
    },
    loadCategories (month, showEmptyCategories) {
      this.groups = []
      categoryService
        .categoriesMonthYear(month, showEmptyCategories)
        .then(groups => {
          this.monthYear = month
          this.groups = groups
        })
    },
    listCards (monthYear) {
      const pattern = 'YYYY-MM'
      const month = moment(monthYear, pattern).subtract(1, 'month').format(pattern)
      return cardService.listCards(month)
        .then(cards => {
          this.cards = cards
          return cards
        })
        .then(this.calculateCards)
    },

    /**
     * @param {[{
     *  expensed: number
     * }]} cards
     */
    calculateCards (cards) {
      this.resumeCards = cards.reduce((resume, card) => {
        resume.planning += card.planning || 0

        resume.launches.push({
          value: card.expensed
        })
        return resume
      }, {
        title: cards.length > 1 ? 'Cartões' : 'Cartão',
        type: 'expense',
        launches: [],
        planning: 0
      })
    },

    removeCard (cardId) {
      cardService.removeCard(cardId)
        .then(() => {
          const index = this.cards.findIndex(card => card.id === cardId)
          this.cards.splice(index, 1)
        })
        .then(() => this.calculateCards(this.cards))
    }
  }
}
