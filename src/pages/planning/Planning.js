import Category from './components/categories/Categories.vue'
import Result from '@/components/result/Result.vue'
import PlanningService from '@/services/planning/PlanningService.js'
import moment from 'moment'

const planningService = new PlanningService()

export default {
  components: {
    category: Category,
    result: Result
  },
  created: function () {
    this.loadPlanning()
  },
  data: () => ({
    categories: []
  }),
  methods: {
    newCategory () {
      this.categories.push({
        monthYear: this.monthYear,
        content: 'config',
        launches: [],
        position: this.categories.length + 1,
        color: '#FF0000',
        planning: 0
      })
    },
    loadPlanning () {
      planningService
        .loadsPlanning(moment().format('yyyy-MM'))
        .then(categories => (this.categories = categories))
    }
  }
}
