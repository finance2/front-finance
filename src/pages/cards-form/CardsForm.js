import CardService from '@/services/card/CardService.js'

const cardService = new CardService()

export default {

  created: function () {
    const cardId = this.$route.params.id

    if (cardId) {
      cardService.cardById(cardId)
        .then(card => {
          this.form.id = card.id
          this.form.title = card.title
          this.form.expensed = card.expensed
          this.form.owner = card.owner
          this.form.number = card.number
          this.form.flag = card.flag

          const [a, b] = card.colors.split(',')
          this.color.a.hex = a
          this.color.b.hex = b
        })
    }
  },

  computed: {
    flag: function () {
      return require(`@/assets/${this.form.flag}.png`)
    },

    bkColor: function () {
      this.form.colors = `${this.color.a.hex},${this.color.b.hex}`
      return {
        background: `linear-gradient(45deg, ${this.form.colors})`
      }
    },

    colorA: {
      get () {
        return this.color.a.hex
      },

      set (value) {
        this.color.a.hex = value
      }
    },

    colorB: {
      get () {
        return this.color.b.hex
      },

      set (value) {
        this.color.b.hex = value
      }
    }
  },

  data: () => ({
    color: { a: { hex: '#000' }, b: { hex: '#444' } },
    flags: [
      {
        text: 'MasterCard',
        value: 'mastercard',
        flag: require(`@/assets/mastercard.png`)
      },
      {
        text: 'Visa',
        value: 'visa',
        flag: require(`@/assets/visa.png`)
      }],
    form: {
      title: '',
      expensed: 0,
      owner: '',
      number: '',
      flag: 'mastercard',
      colors: `#000,#585656`
    }
  }),

  methods: {
    save () {
      this.isNewCard()
        ? this._createCard(this.form)
        : this._updateCard(this.form)
    },

    _createCard (card) {
      cardService.createCard(card)
        .then(response => console.info(`Card ${card} created with id ${response}`))
        .then(this.back)
    },

    _updateCard (card) {
      cardService.updateCard(card)
        .then(() => console.log(`Card ${card} updated`))
        .then(this.back)
    },

    back () {
      this.$router.go(-1)
    },

    isNewCard () {
      return !this.form.id
    }
  }
}
