import PanelTable from './components/panel-table/PanelTable.vue'
import Balance from './components/balance/Balance.vue'
import ModalDebt from './components/modal-debt/ModalDebt.vue'
import StatusBudget from './components/status-budget/StatusBudget.vue'
import CategoryService from '../../services/category/CategoryService'
import DebtService from '../../services/debts/DebtService'

const categoryService = new CategoryService()
const debtService = new DebtService()

export default {
  components: {
    'panelTable': PanelTable,
    'balance': Balance,
    'modalDebt': ModalDebt,
    'statusBudget': StatusBudget
  },
  created () {
    this._prepareBalance()
    categoryService
      .loadCategoriesCurrentMonth()
      .then(categories => {
        console.log(categories)
        this.categorias = categories
      })
  },
  data () {
    return {
      monthYear: new Date().toISOString().substr(0, 10),
      categorias: [],
      debts: debtService.debtsByMonth(),
      debt: {},
      balance: {}
    }
  },
  methods: {
    selectMonth (month) {
      this.monthYear = month
      this.categorias = categoryService.loadCategoriesByMonth(month)
      this.debts = debtService.debtsByMonth(month)
    },
    isEmpty (array) {
      return !array || array.length === 0
    },
    totalDebt (debt) {
      return (debt.totalInstallment - debt.installmentNumber) * debt.value
    },
    _filterDebts (debt) {
      let debtMonthYear = debt.date.toISOString().substr(0, 7)
      return debtMonthYear === this.monthYear.substr(0, 7)
    },
    _prepareBalance () {
      const launches = categoryService.categores.map(category => {
        const launches = category.launches
        return {
          id: category.id,
          description: category.title,
          planning: launches.reduce((sum, launch) => (sum += launch.planning), 0),
          current: launches.reduce((sum, launch) => (sum += launch.current), 0),
          status: launches.some(launch => launch.status === 'Scheduled') ? 'Scheduled' : 'Paid',
          type: category.type
        }
      })
      this.balance = {
        id: 1,
        title: 'Saldo',
        colors: ['#89bf06FF', '#548033FF'],
        monthYear: '2020-12',
        launches
      }
    },
    saveDebt (debt) {
      this.debts.push(debt)
      debtService.saveDebt(debt)
    },
    onChangeStatusDebt (debt) {
      debtService.updateDebt(debt)
    }
  }
}
