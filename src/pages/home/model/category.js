export default {
  id: Number,
  title: String,
  colors: Array,
  launches: Array,
  type: String
}
