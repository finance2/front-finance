import { nextWeekDate } from '../../../../support/util/date-util'

import moment from 'moment'

export default {
  data: () => ({
    dialog: false,
    debt: {},
    today: new Date().toISOString()
  }),
  methods: {
    calculateCurrentNumberlDebt (monthYear) {
      this.debt.installmentNumber = moment().diff(monthYear, 'month') + 1
    },
    saveDebt () {
      this.debt.date = nextWeekDate()
      this.debt.status = 'Scheduled'
      this.$emit('saveDebt', this.debt)
      this.debt = {}
      this.dialog = false
    }
  }
}
