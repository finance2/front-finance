import StatusBudget from '../status-budget/StatusBudget.vue'
import { EVENT_BUS } from '@/support/util/event-bus.js'

export default {
  components: {
    'statusBudget': StatusBudget
  },
  props: {
    category: Object,
    readOnlyStatus: Boolean
  },
  methods: {
    totalBy (field) {
      return this.category.launches.reduce((sum, value) => {
        sum += value[field]
        return sum
      }, 0)
    },
    getPercent (launch) {
      return Math.floor((launch.current / launch.planning) * 100)
    },
    getPercentTotal () {
      return this.getPercent({
        current: this.totalBy('current'),
        planning: this.totalBy('planning')
      })
    },
    getClassColorPercent (launch) {
      if (this.category.type === 'EXPENSE') {
        return this.getPercent(launch) <= 100 ? 'percent-green' : 'percent-red'
      } else {
        return this.getPercent(launch) >= 100 ? 'percent-green' : 'percent-red'
      }
    },
    getClassColorPercentTotal () {
      return this.getClassColorPercent({
        current: this.totalBy('current'),
        planning: this.totalBy('planning')
      })
    },
    isSchedule (launch) {
      return launch.status.toLowerCase() === 'scheduled'
    },
    statusOnChange (launch) {
      const isAllPaid = this.category.launches.every(launch => launch.status === 'Paid')

      const data = {
        status: isAllPaid ? 'Paid' : 'Scheduled',
        id: this.category.id,
        launch
      }

      EVENT_BUS.$emit('allPaid', data)
    }
  }
}
