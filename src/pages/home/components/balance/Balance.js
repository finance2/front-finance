import StatusBudget from '../status-budget/StatusBudget.vue'
import { EVENT_BUS } from '@/support/util/event-bus.js'

export default {
  components: {
    'statusBudget': StatusBudget
  },
  props: {
    data: Object
  },
  created () {
    EVENT_BUS.$on('allPaid', data => {
      if (this.data.launches) {
        const launch = this.data.launches.find(({ id }) => data.id === id)
        if (launch) {
          launch.status = data.status
          EVENT_BUS.$emit('onChangeStatus', data)
        }
      }
    })
  },
  methods: {
    isExpense (launch) {
      return launch.type === 'EXPENSE'
    },
    getTotal () {
      return this.data.launches.reduce((sum, launch) => {
        sum += this.isExpense(launch) ? -launch.current : launch.current
        return sum
      }, 0)
    },
    getTotalClass () {
      const total = this.getTotal()
      return total > 0
        ? 'currency-green'
        : total < 0
          ? 'currency-red'
          : 'currency-blue'
    },
    isSchedule (launch) {
      return launch.status.toLowerCase() === 'scheduled'
    }
  }
}
