import LaunchService from '../../../../services/launch/LaunchService'

const launchService = new LaunchService()

export default {
  props: {
    launch: Object,
    category: Object,
    readOnlyStatus: Boolean
  },
  data: () => ({}),
  methods: {
    isSchedule () {
      return this.launch.status.toLowerCase() === 'scheduled'
    },
    setStatus () {
      this.launch.status = this.isSchedule(this.launch) ? 'Paid' : 'Scheduled'
      launchService
        .saveLaunch(this.launch)
        .then(() => this.$emit('setStatus', this.launch))
    }
  }
}
