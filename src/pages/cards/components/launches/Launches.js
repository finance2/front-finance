import CardService from '@/services/card/CardService.js'

const cardService = new CardService()

export default {
  props: ['changeContent', 'launches', 'groupMode'],

  data: () => ({
    card: undefined
  }),

  computed: {
    groups () {
      return this.launches.reduce((groups, launch) => {
        groups.has(launch.category)
          ? groups.get(launch.category).push(launch)
          : groups.set(launch.category, [launch])

        return groups
      }, new Map())
    }
  },

  methods: {
    /**
     *
     * @param {[*]} values
     */
    isEmpty (values) {
      return !values || values.length === 0
    },

    removeLaunch (launch, index) {
      cardService.removeCardLaunch(launch.id)
        .then(() => this.launches.splice(index, 1))
        .then(() => this.changeContent('launches'))
    },

    /**
     * @param {[{ value: number }]} launches
     */
    total (launches) {
      return launches.reduce((total, launch) => (total += launch.value), 0)
    }
  }
}
