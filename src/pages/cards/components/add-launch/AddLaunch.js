import moment from 'moment'
import FormValidator from './FormValidator.js'
import CardService from '@/services/card/CardService.js'

const cardService = new CardService()

export default {
  props: ['editLaunch', 'changeContent', 'launches'],

  created () {
    this.validator = new FormValidator()
    this.formLaunch = Object.assign({}, this.editLaunch)
    this.formLaunch.date = moment(new Date()).format('YYYY-MM-DD')
  },

  computed: {
    valueInstallment () {
      return (this.formLaunch.value * this.formLaunch.setting.total_installments) | 0
    }
  },

  data: () => ({
    formLaunch: {},
    frequencies: [
      'Semanal',
      'Quinzenal',
      'Mensal',
      'Bimestral',
      'Trimestral',
      'Semestral',
      'Anual'
    ],
    optionsFrequency: [
      { value: 'unique', label: 'Sem término' },
      { value: 'parcel', label: 'Será parcelado' },
      { value: 'repeat', label: 'Será repetido (mesmo valor)' }
    ]
  }),

  methods: {
    async save () {
      const valid = await this.$refs.form.validate()

      if (valid) {
        if (this.isNewLaunch(this.formLaunch)) this._saveNewLaunch(this.formLaunch)
        else this.updateChangedLaunch(this.formLaunch)
      }
    },

    isNewLaunch (launch) {
      return !launch.id
    },

    _saveNewLaunch (launch) {
      cardService.addLaunch(launch)
        .then(launch => {
          this.launches.push(launch)
          this.changeContent('launches')
        })
    },

    updateChangedLaunch (launchChanged) {
      cardService.updateLaunch(launchChanged)
        .then(_ => {
          const index = this.launches.findIndex(launch => launch.id === launchChanged.id)
          this.launches[index] = launchChanged
          this.changeContent('launches')
        })
    },

    changeOption (frequencyOption) {
      if (frequencyOption === 'unique') {
        delete this.formLaunch.setting.current_installment
      } else {
        this.formLaunch.setting.current_installment = 1
      }
    },

    selectDate (date) {
      this.formLaunch.date = moment(date, 'DD/MM/YYYY').format('YYYY-MM-DD')
    },

    showIf (...options) {
      return options.includes(this.formLaunch.setting.option_frequency)
    },

    goBack () {
      this.changeContent('launches')
    }
  }
}
