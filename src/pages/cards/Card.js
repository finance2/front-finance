import moment from 'moment'

import Card from '@/components/card/Card.vue'
import CardService from '@/services/card/CardService.js'

import Launches from './components/launches/Launches.vue'
import AddLaunch from './components/add-launch/AddLaunch.vue'

const cardService = new CardService()

export default {
  components: {
    card: Card,
    launches: Launches,
    addLaunch: AddLaunch
  },

  beforeMount: function () {
    this.currentContent = 'launches'
  },

  created: function () {
    const cardId = this.$route.params.id
    const monthYear = moment(new Date()).format('YYYY-MM')

    cardService.cardById(cardId)
      .then(card => (this.card = card))
      .then(() => this.listLaunchesCard(monthYear, cardId))
  },

  data: () => ({
    groupMode: false,
    editLaunch: undefined,
    card: undefined,
    currentContent: undefined,
    cardLaunches: []
  }),

  methods: {
    registerLaunch () {
      this.editLaunch = {
        card_id: this.card.id,
        setting: {
          frequency: 'Mensal',
          option_frequency: 'repeat',
          total_installments: 1
        }
      }
      this.changeContent('addLaunch')
    },

    listLaunchesCard (month, cardId) {
      const pattern = 'YYYY-MM'
      const monthYear = moment(month, pattern).subtract(1, 'month').format(pattern)
      cardId = cardId || this.card.id
      cardService.launchesByCardId(cardId, monthYear)
        .then(launches => {
          this.cardLaunches = launches
        })
        .then(this._recalculateTotalCard)
    },

    updateLaunch (launch) {
      this.editLaunch = Object.assign({},
        launch,
        { setting: { ...launch.setting } })
      this.changeContent('addLaunch')
    },

    changeContent (contentName) {
      if (contentName === 'launches') this._recalculateTotalCard()

      this.currentContent = contentName
    },

    _recalculateTotalCard () {
      this.card.expensed = this.cardLaunches.reduce((total, launch) => total + launch.value, 0)
    },

    back () {
      this.$router.go(-1)
    }
  }
}
