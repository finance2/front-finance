import moment from 'moment'

const GlobalFilters = {
  install (Vue) {
    Vue.filter('currency', function (value) {
      value = !value ? 0 : value
      return value.toLocaleString('pt-BR', {style: 'currency', currency: 'BRL'})
    })
    Vue.filter('percent', function (value) {
      if (value) {
        return `${(value * 100).toFixed(2)} %`
      }
    })
    Vue.filter('formateDate', function (date, pattern) {
      if (date instanceof String) {
        date = new Date(Number.parseInt(date))
      }
      return moment(date).format(pattern)
    })
    Vue.filter('capitalize', function (value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    })
    Vue.filter('uppercase', function (value) {
      if (!value) return ''
      return value.toString().toUpperCase()
    })
  }
}

export default GlobalFilters
