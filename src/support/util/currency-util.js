const currencyFormatter = new Intl.NumberFormat('pt-BR', {
  currency: 'BRL',
  minimumFractionDigits: 2,
  maximumFractionDigits: 2
})

const fromCurrency = (value) => {
  return !value
    ? 0.00
    : String(value)
      .replace(/\D/gm, '') * 0.01
}

module.exports = {
  format: currencyFormatter.format,
  fromCurrency
}
