import Vue from 'vue'
import Router from 'vue-router'

// import Home from '@/pages/home/Home.vue'
import Dashboard from '@/pages/dashboard/Dashboard.vue'
import Planning from '@/pages/planning/Planning.vue'
import Card from '@/pages/cards/Card.vue'
import CardForm from '@/pages/cards-form/CardsForm.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'home',
      path: '',
      redirect: 'dashboard'
    },
    {
      name: 'dashboard',
      path: '/dashboard',
      component: Dashboard
    },
    {
      name: 'planning',
      path: '/planning',
      component: Planning
    },
    {
      name: 'cards-form',
      path: '/cards/form',
      component: CardForm
    },
    {
      name: 'cards-form-edit',
      path: '/cards/form/:id',
      component: CardForm
    },
    {
      name: 'cards',
      path: '/cards/:id',
      component: Card
    }
  ]
})
