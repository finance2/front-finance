import axios from 'axios'

import {
  ALL_CATEGORIES_WITH_LAUNCHES,
  ALL_CATEGORIES,
  SAVE_CATEGORY,
  TOTAL_BY_CATEGORY,
  ADD_LAUNCH
} from './graphql'

const GRAPHQL_URL = 'http://0.0.0.0:4000'

export default {

  allFinances () {
    let query = {
      query: ALL_CATEGORIES_WITH_LAUNCHES
    }
    return axios.post(GRAPHQL_URL, query)
      .then(res => res.data.data)
      .catch(error => console.error(error))
  },
  saveLaunche (launch) {
    let query = {
      query: ADD_LAUNCH(launch)
    }
    return axios.post(GRAPHQL_URL, query)
      .then(res => {
        let launchAdded = res.data.data.addLaunche
        launchAdded.category = launch.category
        return launchAdded
      })
      .catch(error => console.error(error))
  },
  saveCategory (category) {
    let query = {
      query: SAVE_CATEGORY(category)
    }
    return axios.post(GRAPHQL_URL, query)
      .then(res => res.data.data.createCategory)
      .catch(error => console.error(error))
  },
  allCategories () {
    let query = {
      query: ALL_CATEGORIES
    }
    return axios.post(GRAPHQL_URL, query)
      .then(res => res.data.data)
      .catch(error => console.error(error))
  },
  totalByCategory () {
    let query = {
      query: TOTAL_BY_CATEGORY
    }
    return axios.post(GRAPHQL_URL, query)
      .then(res => res.data.data)
      .catch(error => console.error(error))
  }
}
