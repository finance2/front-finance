export const ALL_CATEGORIES_WITH_LAUNCHES = `{
  categories {
    id
    name
    type
    planned
    launches {
      id
      date
      description
      status
      value
    }
  }
}`

export const ALL_CATEGORIES = `{
  categories {
    id
    name
  }
}`

export const TOTAL_BY_CATEGORY = `{
  categories {
    name
    planned
    type
  }
}`

export const ADD_LAUNCH = (launche) => {
  return `mutation {addLaunche(
    category_id: "${launche.category}"
    date: "${launche.date}"
    description: "${launche.description}"
    status: ${launche.status}
    value: ${launche.value}
  ) {
    id
    date
    description
    status
    value
  }}`
}

export const SAVE_CATEGORY = (category) => {
  return `mutation {createCategory(
    name: "${category.name}"
    type: ${category.type}
    planned: ${category.planned}
  ) {
    name
    type
    planned
  }}`
}
