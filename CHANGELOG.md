
## 0.1.2 (2023-01-09)

### fixed (1 change)

- [Moves license file](finance2/front-finance@126e9c7dd4a4e06aa95302e572de8c89aead8f38)

### added (1 change)

- [Adds license file](finance2/front-finance@585d7eb7878509cb5b30f1f9ae8637ace6d4be3b)

### changed (1 change)

- [Renames file to md](finance2/front-finance@ab175071a126f26d51d7c52c0bfb2f22f9ff7658)

### removed (1 change)

- [Removes comments](finance2/front-finance@fcb58cc5b263449797bc431e8b364e76813adf44) ([merge request](finance2/front-finance!10))

## 0.1.1 (2023-01-09)

### fixed (2 changes)

- [Moves license file](finance2/front-finance@126e9c7dd4a4e06aa95302e572de8c89aead8f38)
- [Improves comments](finance2/front-finance@2f77fe609ed4ecf327e08e50ec2ea78f40c3cb55)

### added (2 changes)

- [Adds license file](finance2/front-finance@585d7eb7878509cb5b30f1f9ae8637ace6d4be3b)
- [Adds new comment](finance2/front-finance@cbee1039ddbf39dddcd4594b66b115ec3c3d6800)

### changed (1 change)

- [Renames file to md](finance2/front-finance@ab175071a126f26d51d7c52c0bfb2f22f9ff7658)

### removed (1 change)

- [Removes comments](finance2/front-finance@fcb58cc5b263449797bc431e8b364e76813adf44) ([merge request](finance2/front-finance!10))
